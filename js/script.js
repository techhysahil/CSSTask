var progressBar = document.getElementById("js_progress_bar");
var progress = document.getElementById("js_progress");
var popover = document.getElementById("js_popover");
var giveNow = document.getElementById("js_giveNow");

var givenValue = document.getElementById("js_givenValue").value;
progress.style.width=((givenValue*100)/5000) + "%";


progressBar.addEventListener("mouseenter", function(){
	console.log("hover in");
	popover.className += " show";
});

progressBar.addEventListener("mouseleave", function(){
	console.log("hover out");
	popover.className = "popover_wrapper";
});

giveNow.addEventListener("click", function(){
	var givenValue = document.getElementById("js_givenValue").value;
	progress.style.width=((givenValue*100)/5000) + "%";
});